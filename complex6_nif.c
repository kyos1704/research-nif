#include "erl_nif.h"
#include <string.h>
#include<stdio.h>
extern int foo(int x);
extern int bar(int y);
extern char string_to_char(char a[]);
extern int string_to_int(char a[]);



static ERL_NIF_TERM string_to_int_nif(ErlNifEnv* env,int argc,const ERL_NIF_TERM argv[]){
  char x[10];
  int ret;
  int size;
  if(!enif_get_string(env,argv[0],x,10,ERL_NIF_LATIN1)){
    return enif_make_badarg(env);
  }
  ret = string_to_int(x);
  return enif_make_int(env,ret);
}

static ERL_NIF_TERM string_to_char_nif(ErlNifEnv* env,int argc,const ERL_NIF_TERM argv[]){
  char x[10];
  char tmp ;
  if(!enif_get_string(env,argv[0],x,10,ERL_NIF_LATIN1)){
    return enif_make_badarg(env);
  }
  tmp = string_to_char(x);
  return enif_make_string(env,&tmp,ERL_NIF_LATIN1);
}


static ERL_NIF_TERM foo_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    int x, ret;
    if (!enif_get_int(env, argv[0], &x)) {
      return enif_make_badarg(env);
    }
    ret = foo(x);
    return enif_make_int(env, ret);
}

static ERL_NIF_TERM bar_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    int y, ret;
    if (!enif_get_int(env, argv[0], &y)) {
      return enif_make_badarg(env);
    }
    ret = bar(y);
    return enif_make_int(env, ret);
}

static ErlNifFunc nif_funcs[] = {
    {"foo", 1, foo_nif},
    {"bar", 1, bar_nif},
    {"string_to_char", 1, string_to_char_nif},
    {"string_to_int",1,string_to_int_nif}
};

ERL_NIF_INIT(complex6, nif_funcs, NULL, NULL, NULL, NULL)

